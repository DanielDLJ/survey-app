import React from "react";
import { Button } from "react-native";
import { useNavigation } from "@react-navigation/native";

const LoginButton: React.FC = () => {
  const navigation = useNavigation();

  const handleLogin = () => {
    console.log("Login button pressed");
  };

  return <Button title="Login" onPress={handleLogin} />;
};

export default LoginButton;
