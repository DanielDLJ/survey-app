import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

interface SurveyListItemProps {
  title: string;
  description: string;
  active: boolean;
  onEdit?: () => void;
  onDeactivate?: () => void;
  onActivate?: () => void;
}

const SurveyListItem: React.FC<SurveyListItemProps> = ({
  title,
  description,
  active,
  onEdit,
  onDeactivate,
  onActivate,
}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.itemTitle}>{title}</Text>
      <Text>{description}</Text>
      {onEdit && (
        <TouchableOpacity style={styles.btns} onPress={onEdit}>
          <Text> Edit </Text>
        </TouchableOpacity>
      )}
      {onDeactivate && active && (
        <TouchableOpacity style={styles.btns} onPress={onDeactivate}>
          <Text>Deactivate</Text>
        </TouchableOpacity>
      )}
      {onActivate && !active && (
        <TouchableOpacity style={styles.btns} onPress={onActivate}>
          <Text>Activate</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    marginBottom: 15,
    // padding: 10,
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 5,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
  },
  itemTitle: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 5,
  },
  btns: {
    backgroundColor: "lightblue",
    color: "white",
    padding: 5,
    margin: 5,
    width: 80,
    textAlign: "center",
    alignItems: "center",
    borderRadius: 5,
  },
});

export default SurveyListItem;
