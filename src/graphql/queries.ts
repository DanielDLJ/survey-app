// src/graphql/queries.ts

import { gql } from "@apollo/client";

export const LOGIN_MUTATION = gql`
  mutation Login($input: LoginInput) {
    login(input: $input) {
      id
      name
      email
      userJwtToken {
        token
      }
      role
    }
  }
`;

interface LoginInput {
  email: string;
  password: string;
}

export const SURVEYS_QUERY = gql`
  query Surveys {
    surveys {
      id
      title
      description
      questions {
        id
        surveyId
        questionText
        type
        options
        required
      }
      active
      createdAt
    }
  }
`;

export const EDIT_SURVEY = gql`
  mutation EditSurvey($editSurveyId: ID!, $input: SurveyEditInput!) {
    editSurvey(id: $editSurveyId, input: $input) {
      id
      title
      description
      active
      createdAt
      questions {
        id
        surveyId
        questionText
        type
        options
        required
      }
    }
  }
`;
