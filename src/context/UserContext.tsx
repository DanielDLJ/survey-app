import React, { createContext, useContext, useEffect, useState } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Alert } from "react-native";

interface User {
  id: string;
  name: string;
  email: string;
  role: string;
  token: string;
}

interface UserContextType {
  user: User | null;
  setUser: (user: User | null) => Promise<void>;
  logout: () => void;
}

const UserContext = createContext<UserContextType | undefined>(undefined);

export const useUser = (): UserContextType => {
  const context = useContext(UserContext);
  if (!context) {
    throw new Error("useUser deve ser usado dentro de um UserProvider");
  }
  return context;
};

export const UserProvider: React.FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [user, setUserState] = useState<User | null>(null);

  const storeData = async (value: string) => {
    try {
      await AsyncStorage.setItem("token", value);
    } catch (e) {
      Alert.alert("Erro", "Erro ao login");
      console.log("storeData", e);
    }
  };

  const setUser = async (user: User | null) => {
    setUserState(user);
    if (user) {
      await storeData(user.token);
    }
  };

  const logout = async () => {
    setUserState(null);
    await AsyncStorage.removeItem("token");
  };

  return (
    <UserContext.Provider value={{ user, setUser, logout }}>
      {children}
    </UserContext.Provider>
  );
};
