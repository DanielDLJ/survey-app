import React from "react";
import { View, Text, StyleSheet, FlatList, Button, Alert } from "react-native";
import SurveyListItem from "../components/SurveyListItem";
import { EDIT_SURVEY, SURVEYS_QUERY } from "../graphql/queries";
import { useMutation, useQuery } from "@apollo/client";

const AdminSurveyListScreen: React.FC = () => {
  const { loading, error, data } = useQuery(SURVEYS_QUERY);
  const [editMutation] = useMutation(EDIT_SURVEY);

  if (loading) return <Text>Carregando...</Text>;
  if (error) {
    console.log(error);
    return <Text>Erro ao carregar os surveys.</Text>;
  }

  const surveys = data.surveys;

  const handleEditSurvey = (id: string) => {
    // Implemente a lógica de edição aqui
  };

  const handleAddSurvey = () => {
    // Implemente a lógica de adição aqui
  };

  const handleDeactivateSurvey = async (id: string) => {
    try {
      const { data } = await editMutation({
        variables: {
          editSurveyId: id,
          input: {
            active: false,
          },
        },
      });
      Alert.alert("Sucesso", "Pesquisa desativada com sucesso.");
    } catch (error) {
      console.log(error);
      Alert.alert("Erro", "Erro ao desativar a pesquisa.");
    }
  };

  const handleActivateSurvey = async (id: string) => {
    try {
      const { data } = await editMutation({
        variables: {
          editSurveyId: id,
          input: {
            active: true,
          },
        },
      });
      Alert.alert("Sucesso", "Pesquisa ativada com sucesso.");
    } catch (error) {
      console.log(error);
      Alert.alert("Erro", "Erro ao ativar a pesquisa.");
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Admin Survey Lis</Text>
      <FlatList
        data={surveys}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <SurveyListItem
            title={item.title}
            description={item.description}
            active={item.active}
            onEdit={() => handleEditSurvey(item.id)}
            onDeactivate={() => handleDeactivateSurvey(item.id)}
            onActivate={() => handleActivateSurvey(item.id)}
          />
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
  },
  item: {
    marginBottom: 15,
    padding: 10,
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 5,
  },
  itemTitle: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 5,
  },
});

export default AdminSurveyListScreen;
