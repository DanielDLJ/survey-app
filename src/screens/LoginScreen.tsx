import React, { useState } from "react";
import { View, Text, StyleSheet, TextInput, Button, Alert } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { useMutation } from "@apollo/client";
import { LOGIN_MUTATION } from "../graphql/queries";
import { useUser } from "../context/UserContext";

interface LoginInput {
  email: string;
  password: string;
}

const LoginScreen: React.FC = () => {
  const navigation = useNavigation();
  const [email, setEmail] = useState<string>("beto@hotmail.com");
  const [password, setPassword] = useState<string>("123456");
  const [emailAdmin, setEmailAdmin] = useState<string>("ana@hotmail.com");
  const [passwordAdmin, setPasswordAdmin] = useState<string>("123456");
  const { setUser } = useUser();

  const [loginMutation] = useMutation(LOGIN_MUTATION);

  const handleLogin = async () => {
    try {
      const { data } = await loginMutation({
        variables: {
          input: {
            email,
            password,
          },
        },
      });

      const {
        id,
        name,
        email: userEmail,
        role,
        userJwtToken: { token },
      } = data.login;

      const user = {
        id,
        name,
        email: userEmail,
        role,
        token,
      };
      console.log(data);
      await setUser(user);

      if (role === "ADMIN") {
        navigation.navigate("AdminSurveyList" as never);
      } else {
        navigation.navigate("UserSurveyList" as never);
      }
    } catch (error) {
      console.log(error);
      Alert.alert("Erro", "E-mail ou senha inválidos.");
    }
  };

  const handleLoginAdmin = async () => {
    try {
      const { data } = await loginMutation({
        variables: {
          input: {
            email: emailAdmin,
            password: passwordAdmin,
          },
        },
      });

      const {
        id,
        name,
        email: userEmail,
        role,
        userJwtToken: { token },
      } = data.login;

      const user = {
        id,
        name,
        email: userEmail,
        role,
        token,
      };
      console.log(data);
      await setUser(user);

      if (role === "ADMIN") {
        navigation.navigate("AdminSurveyList" as never);
      } else {
        navigation.navigate("UserSurveyList" as never);
      }
    } catch (error) {
      console.log(error);
      Alert.alert("Erro", "E-mail ou senha inválidos.");
    }
  };

  return (
    <View style={styles.container}>
      <Text>Login normal</Text>
      <TextInput
        style={styles.input}
        placeholder="E-mail"
        value={email}
        onChangeText={setEmail}
        keyboardType="email-address"
        autoCapitalize="none"
        // autoCompleteType="email"
      />
      <TextInput
        style={styles.input}
        placeholder="Senha"
        value={password}
        onChangeText={setPassword}
        secureTextEntry
        autoCapitalize="none"
        // autoCompleteType="password"
      />
      <Button title="Entrar" onPress={handleLogin} />
      <Text>Login Admin</Text>
      <TextInput
        style={styles.input}
        placeholder="E-mail"
        value={emailAdmin}
        onChangeText={setEmailAdmin}
        keyboardType="email-address"
        autoCapitalize="none"
        // autoCompleteType="email"
      />
      <TextInput
        style={styles.input}
        placeholder="Senha"
        value={passwordAdmin}
        onChangeText={setPasswordAdmin}
        secureTextEntry
        autoCapitalize="none"
        // autoCompleteType="password"
      />
      <Button title="Entrar" onPress={handleLoginAdmin} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 16,
  },
  input: {
    width: "100%",
    height: 40,
    marginVertical: 10,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: "#ccc",
  },
});

export default LoginScreen;
