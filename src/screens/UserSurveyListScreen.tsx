import React from "react";
import { View, Text, FlatList, StyleSheet } from "react-native";
import { useQuery } from "@apollo/client";
import { SURVEYS_QUERY } from "../graphql/queries";
import SurveyListItem from "../components/SurveyListItem";

const UserSurveysListScreen: React.FC = () => {
  const { loading, error, data } = useQuery(SURVEYS_QUERY);

  if (loading) return <Text>Carregando...</Text>;
  if (error) {
    console.log(error);
    return <Text>Erro ao carregar os surveys.</Text>;
  }

  const surveys = data.surveys;

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Lista de Surveys</Text>
      <FlatList
        data={surveys}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <SurveyListItem
            title={item.title}
            description={item.description}
            active={item.active}
          />
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
  },
  item: {
    marginBottom: 15,
    padding: 10,
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 5,
  },
  itemTitle: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 5,
  },
});

export default UserSurveysListScreen;
