import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import {
  ApolloClient,
  ApolloLink,
  ApolloProvider,
  HttpLink,
  InMemoryCache,
} from "@apollo/client";
import client from "./src/graphql";
import { UserProvider } from "./src/context/UserContext";
import LoginScreen from "./src/screens/LoginScreen";
import UserSurveyListScreen from "./src/screens/UserSurveyListScreen";
import AdminSurveyListScreen from "./src/screens/AdminSurveyListScreen";
import { Button } from "react-native";
import { useUser } from "./src/context/UserContext";

const Stack = createStackNavigator();

const App: React.FC = () => {
  const { setUser } = useUser();
  return (
    <ApolloProvider client={client}>
      <UserProvider>
        <NavigationContainer>
          <Stack.Navigator initialRouteName="Login">
            <Stack.Screen name="Login" component={LoginScreen} />
            <Stack.Screen
              name="UserSurveyList"
              component={UserSurveyListScreen}
            />
            <Stack.Screen
              name="AdminSurveyList"
              component={AdminSurveyListScreen}
              options={({ navigation }) => ({
                headerRight: () => (
                  <Button
                    title="Logout"
                    onPress={() => {
                      navigation.navigate("Login");
                    }}
                  />
                ),
              })}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </UserProvider>
    </ApolloProvider>
  );
};

export default App;
